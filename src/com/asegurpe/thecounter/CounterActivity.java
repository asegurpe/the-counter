package com.asegurpe.thecounter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class CounterActivity extends Activity {
	
	private static final String COUNTER = "counter";

	private int counter = 0;
	
	private Button incButton;
	private Button deductButton;
	private Button resetButton;
	private TextView counterText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		incButton = (Button) findViewById(R.id.inc);
		deductButton = (Button) findViewById(R.id.deduct);
		resetButton = (Button) findViewById(R.id.reset);
		counterText = (TextView) findViewById(R.id.counter);

		incButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				counter++;
				updateCounterTextView();
				deductButton.setEnabled(true);
			}

		});
		
		deductButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (counter > 0) {
					counter--;
				}
				if (counter == 0) {
					deductButton.setEnabled(false);
				}
				updateCounterTextView();
			}
			
		});
		
		resetButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				resetCounter();
			}
		});
		
		deductButton.setEnabled(false);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		if (savedInstanceState != null) {
			savedInstanceState.putInt(COUNTER, counter);
		}
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState != null && savedInstanceState.containsKey(COUNTER)) {
			counter = (Integer)savedInstanceState.getInt(COUNTER);
			counterText.setText(Integer.toString(counter));
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.reset:
			resetCounter();
			break;
		case R.id.about:
			viewAbout();
			break;
		}
		return true;
	}

	/**
	 * Reset counter method
	 */
	private void resetCounter() {
		counter = 0;
		updateCounterTextView();
	}
	
	private void updateCounterTextView() {
		counterText.setText(Integer.toString(counter));
	}
	
	private void viewAbout() {
		Intent i = new Intent(this, AboutActivity.class);
		startActivity(i);
	}

}
