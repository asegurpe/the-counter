/**
 * 
 */
package com.asegurpe.thecounter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * @author ASEGURPE
 * 
 */
public class AboutActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		
		Button backButton = (Button)findViewById(R.id.back);
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				back();
			}
		});
	}
	
	private void back() {
		Intent i = new Intent(this, CounterActivity.class);
		startActivity(i);
	}


}
